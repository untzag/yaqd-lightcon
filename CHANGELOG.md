# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/).

## [Unreleased]

### Added
- conda-forge as installation source

### Fixed
- added forgotten config options to is-daemon: enable, log_level, and log_to_file

## [2020.11.0]

### Changed
- regenerated avpr based on recent traits update
- use new trait system in core

## [2020.07.0]

### Changed
- Now uses Avro-RPC [Yep-107](https://yeps.yaq.fyi/107/)
- Use Flit for distribution
- Split topas4 into lightcon-topas4-motor and lightcon-topas4-shutter

## [2020.05.0]

### Added
- initial release

[Unreleased]: https://gitlab.com/yaq/yaqd-horiba/-/compare/v2020.11.0...master
[2020.11.0]: https://gitlab.com/yaq/yaqd-horiba/-/compare/v2020.05.0...v2020.11.0
[2020.07.0]: https://gitlab.com/yaq/yaqd-horiba/-/compare/v2020.05.0...v2020.07.0
[2020.05.0]: https://gitlab.com/yaq/yaqd-horiba/-/tags/v2020.05.0
